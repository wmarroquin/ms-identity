import { LogLevel } from '@azure/msal-browser';

export const msalConfig = {
  auth: {
    clientId: process.env.REACT_APP_CLIENT_ID || '',
    authority: process.env.REACT_APP_AUTHORITY_URL || '',
    redirectUri: process.env.REACT_APP_REDIRECT || '',
  },
  cache: {
    cacheLocation: 'sessionStorage', // This configures where your cache will be stored
    storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
  },
  system: {
    loggerOptions: {
      loggerCallback: (level, message, containsPii) => {
        if (containsPii) {
          return;
        }
        switch (level) {
          case LogLevel.Error:
            console.error({ Error: message });
            return;
          case LogLevel.Info:
            console.info({ Info: message });
            return;
          case LogLevel.Verbose:
            console.debug({ Verbose: message });
            return;
          case LogLevel.Warning:
            console.warn({ Warning: message });
            return;
        }
      },
    },
  },
};

export const loginRequest = {
  scopes: [`api://${process.env.REACT_APP_CLIENT_ID}/user_impersonation`],
};
